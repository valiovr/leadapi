<?php

return [
	'api_key' => env('API_KEY', ''),
	'client_ip_list' => env('CLIENT_IP_LIST', ''),
	'cors_url' => env('CORS_URL', ''),
	'cors_admin_url' => env('CORS_ADMIN_URL', ''),
	'image_upload_dir' => env('IMAGE_UPLOAD_DIR', ''),
];