<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnNumberFieldToMetaObject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Alter la_meta_objects table / Add column_number field
        Schema::table('la_meta_objects', function(Blueprint $table)
        {
            $table->integer('column_number')->after('sub_layer_id')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Drop column_number field
        Schema::table('la_meta_objects', function(Blueprint $table)
        {
            $table->dropColumn('column_number');
        });
    }
}
