<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddObjectCategoryForeignKeyToXbpmnNotations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Alter la_xbpmn_notations table / Add foreign key to roles table
        Schema::table('la_xbpmn_notations', function(Blueprint $table)
        {
            $table->integer('object_category_id')->unsigned()->after('sub_layer_id');
            $table->foreign('object_category_id')
                ->references('id')
                ->on('la_object_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Drop foreign key
        Schema::table('la_xbpmn_notations', function(Blueprint $table)
        {
            $table->dropForeign('la_xbpmn_notations_object_object_category_id_foreign');
            $table->dropColumn('object_class_id');
        });
    }
}
