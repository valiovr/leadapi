<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObjectRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('la_object_relations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('object_id')->unsigned();
            $table->integer('relation_type_id')->unsigned()->nullable();
            $table->integer('related_object_id')->unsigned();
            $table->timestamps();

            $table->foreign('object_id')->references('id')->on('la_meta_objects');
            $table->foreign('relation_type_id')->references('id')->on('la_relation_types');
            $table->foreign('related_object_id')->references('id')->on('la_meta_objects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Drop foreign key for roles table
        Schema::table('la_object_relations', function(Blueprint $table)
        {
            $table->dropForeign('la_object_relations_object_id_foreign');
            $table->dropForeign('la_object_relations_relation_type_id_foreign');
            $table->dropForeign('la_object_relations_related_object_id_foreign');
        });
        Schema::drop('la_object_relations');
    }
}
