<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObjectCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('la_object_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',255);
            $table->integer('object_class_id')->unsigned();
            $table->timestamps();

            //add Foreign Key
            $table->foreign('object_class_id')
                ->references('id')
                ->on('la_object_classes');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('la_object_categories');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
