<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUserObjectChangesForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('la_user_object_changes', function (Blueprint $table) {
            $table->dropForeign('la_user_object_changes_user_object_id_foreign');
            $table->foreign('user_object_id')->references('id')->on('la_user_objects')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
