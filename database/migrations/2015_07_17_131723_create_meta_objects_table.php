<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetaObjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('la_meta_objects', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name',255);
			$table->longText('description');
            $table->string('icon',255);
            $table->integer('l_id')->unsigned()->nullable();
            $table->timestamps();

		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('la_meta_objects');
    }
}
