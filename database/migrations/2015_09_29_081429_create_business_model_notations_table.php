<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessModelNotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('la_business_model_notations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',255);
            $table->longText('description');
            $table->string('notation',255);
            $table->integer('sub_layer_id')->unsigned();
            $table->integer('object_category_id')->unsigned();
            $table->timestamps();

            //add Foreign Key
            $table->foreign('sub_layer_id')
                ->references('id')
                ->on('la_sub_layers');

            $table->foreign('object_category_id')
                ->references('id')
                ->on('la_object_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('business_model_notations');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
