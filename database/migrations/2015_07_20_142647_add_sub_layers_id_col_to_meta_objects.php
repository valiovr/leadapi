<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubLayersIdColToMetaObjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Alter users table / Add foreign key to roles table
        Schema::table('la_meta_objects', function(Blueprint $table)
        {
            $table->integer('sub_layer_id')->unsigned()->after('l_id');
            $table->foreign('sub_layer_id')
                ->references('id')
                ->on('la_sub_layers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Drop foreign key for roles table
        Schema::table('la_meta_objects', function(Blueprint $table)
        {
            $table->dropForeign('la_meta_objects_sub_layer_id_foreign');
            $table->dropColumn('sub_layer_id');
        });
    }
}
