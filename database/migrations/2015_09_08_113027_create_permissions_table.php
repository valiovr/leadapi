<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('la_permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('object_type');
            $table->integer('object_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->boolean('access_allowed')->default(0);
            $table->timestamps();

            $table->foreign('user_id')->references('user_id')->on('mw_user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('la_permissions');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

    }
}
