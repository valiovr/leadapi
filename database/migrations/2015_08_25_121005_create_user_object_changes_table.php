<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserObjectChangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('la_user_object_changes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('icon');
            $table->integer('stereotype_id')->unsigned()->nullable();
            $table->integer('type_id')->unsigned()->nullable();
            $table->integer('subtype_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned();
            $table->integer('meta_object_id')->unsigned();
            $table->integer('user_object_id')->unsigned();

            $table->timestamps();

            $table->foreign('stereotype_id')->references('id')->on('la_object_types');
            $table->foreign('type_id')->references('id')->on('la_object_types');
            $table->foreign('subtype_id')->references('id')->on('la_object_types');
            $table->foreign('user_id')->references('user_id')->on('mw_user');
            $table->foreign('meta_object_id')->references('id')->on('la_meta_objects');
            $table->foreign('user_object_id')->references('id')->on('la_user_objects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('la_user_object_changes');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
