<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObjectTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('la_object_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->longText('description');
            $table->longText('reference');
            $table->integer('object_id')->unsigned();
            $table->integer('parent_id')->default(0);
            $table->timestamps();

            $table->foreign('object_id')->references('id')->on('la_meta_objects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('la_object_types');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
