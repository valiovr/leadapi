<?php

use Illuminate\Database\Seeder;
use LeadApi\SubLayer as SubLayer;

class FillSubLayersTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        //Truncate la_sub_layers table
        DB::table('la_sub_layers')->truncate();

        SubLayer::create([
            'name' => 'Purpose & Goal (Value)',
            'description' => 'Contains the business objects that capture the scope and value of the business.',
            'color'=>'#48d0ff',
            'layer_id'=>1
        ]);
        SubLayer::create([
            'name' => 'Business Competency',
            'description' => 'Contains the business objects that capture and describe the essential organizations skill and knowledge needed to fulfil the scope and purpose of the business.',
            'color'=>'#0b98d6',
            'layer_id'=>1
        ]);
        SubLayer::create([
            'name' => 'Business Service',
            'description' => 'Contains the business objects that realize behaviour.',
            'color'=>'#014480',
            'layer_id'=>1
        ]);
        SubLayer::create([
            'name' => 'Business Process',
            'description' => 'Contains the business objects necessary to execute work and create value.',
            'color'=>'#1f992d',
            'layer_id'=>1
        ]);


        SubLayer::create([
            'name' => 'Application',
            'description' => 'Contains the business objects necessary to describe the structure and behaviour of software that enables work',
            'color'=>'#f7b206',
            'layer_id'=>2
        ]);

        SubLayer::create([
            'name' => 'Data',
            'description' => 'Contains the business objects necessary to describe the persistent information used within the software.',
            'color'=>'#f08d00',
            'layer_id'=>2
        ]);


        SubLayer::create([
            'name' => 'Platform',
            'description' => 'Contains the business objects necessary to provide storage and access to data or to allow applications to execute.',
            'color'=>'#dc5028',
            'layer_id'=>3
        ]);

        SubLayer::create([
            'name' => 'Infrastructure',
            'description' => 'Contains the business objects necessary to provide the environment for the assets within the platform to function or which contrain its operation.',
            'color'=>'#a0281e',
            'layer_id'=>3
        ]);
        //Enable checking Foreign key
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
