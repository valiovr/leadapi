<?php

use Illuminate\Database\Seeder;

use LeadApi\MetaObject as MetaObject;

class FillMetaObjectsTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Truncate la_meta_objects table
        DB::table('la_meta_objects')->truncate();

        MetaObject::create([
            'name' => 'Force (external or internal)',
            'description' => 'An external or internal factor that forces or pushes some aspect of an enterprise in a specific direction.',
            'l_id'=>1,
            'sub_layer_id'=>1,
            'icon'=>'Force-01.png',
            'column_number' => 1
        ]);

        MetaObject::create([
            'name' => 'Driver (external or internal)',
            'description' => 'An external or internal factor that drives, establishes motivation for or influences the direction of an enterprise.',
            'l_id'=>2,
            'sub_layer_id'=>1,
            'icon'=>'Driver-01.png',
            'column_number' => 1
        ]);

        MetaObject::create([
            'name' => 'Vision',
            'description' => 'The desired future state of the enterprise. An imagination of the future aspirational state of how the enterprise could or should be like without regard as to how this will be achieved.',
            'l_id'=>3,
            'sub_layer_id'=>1,
            'icon'=>'Vision-01.png',
            'column_number' => 1
        ]);

        MetaObject::create([
            'name' => 'Mission',
            'description' => 'The purpose and nature of the enterprise.',
            'l_id'=>4,
            'sub_layer_id'=>1,
            'icon'=>'Mission-01.png',
            'column_number' => 1
        ]);
        MetaObject::create([
        'name' => 'Expectation',
        'description' => 'The anticipated benefits that are of worth, importance, and significance to a specific stakeholder.',
        'l_id'=>5,
        'sub_layer_id'=>1,
            'icon'=>'Expectation-01.png',
            'column_number' => 1

        ]);

        MetaObject::create([
            'name' => 'Value Proposition',
            'description' => 'The merit and benefit that a customer, added value partner or the market itself can obtain from their perspective and point of view.',
            'l_id'=>6,
            'sub_layer_id'=>1,
            'icon'=>'Value Proposition-01.png',
            'column_number' => 1
        ]);

        MetaObject::create([
            'name' => 'Strategy (Strategic Business Objective)',
            'description' => 'The direction and ends to which the enterprise seeks as well as the means and methods by which the ends will be attained.',
            'l_id'=>7,
            'sub_layer_id'=>1,
			'icon' =>'Strategy-01.png',
            'column_number' => 3
        ]);

        MetaObject::create([
            'name' => 'Goal (business, application or technology)',
            'description' => 'A desired result considered a part of the organizational direction, aims, targets, and aspirations.',
            'l_id'=>8,
            'sub_layer_id'=>1,
            'icon'=>'Goal-01.png',
            'column_number' => 3
        ]);

        MetaObject::create([
            'name' => 'Objective',
            'description' => "The purpose or target of one's efforts or actions.",
            'l_id'=>9,
            'sub_layer_id'=>1,
            'icon'=>'Objective-01.png',
            'column_number' => 2
        ]);

        MetaObject::create([
            'name' => 'Quality',
            'description' => 'A state of excellence or worth specifying the essential and distinguishing individual nature and the attributes based on the intended use.',
            'l_id'=>10,
            'sub_layer_id'=>1,
            'icon'=>'Quality-01.png',
            'column_number' => 2
        ]);


        MetaObject::create([
            'name' => 'Risk',
            'description' => 'The combined impact of any conditions or events, including those caused by uncertainty, change, hazards or other factors that can affect the potential for achieving objectives.',
            'l_id'=>11,
            'sub_layer_id'=>1,
            'icon'=>'Risk-01.png',
            'column_number' => 2
        ]);

        MetaObject::create([
            'name' => 'Security',
            'description' => 'The objects or tools that secure, make safe and protect through measures to prevent exposure to danger or risk.',
            'l_id'=>12,
            'sub_layer_id'=>1,
            'icon'=>'Security-01.png',
            'column_number' => 2
        ]);

        MetaObject::create([
            'name' => 'Measure',
            'description' => 'Any type of measurement used to gauge some quantifiable component of an enterprise’s performance.',
            'l_id'=>13,
            'sub_layer_id'=>1,
            'icon'=>'Measure-01.png',
            'column_number' => 2
        ]);

        MetaObject::create([
            'name' => 'Report',
            'description' => 'The exposure, description, and portrayal of information about the status, direction or execution of work within the functions, services, processes, and resources of the enterprise.',
            'l_id'=>14,
            'sub_layer_id'=>1,
            'icon'=>'Report-01.png',
            'column_number' => 3
        ]);

        MetaObject::create([
            'name' => 'Time',
            'description' => 'A plan, schedule, arrangement or measure for when something should initiate, take place, be completed or the amount of time consumed.',
            'l_id'=>15,
            'sub_layer_id'=>1,
            'icon'=>'Time-01.png',
            'column_number' => 2
        ]);

        MetaObject::create([
            'name' => 'Monitor',
            'description' => 'To be aware of the state, through observation or measuring.
To supervise and to continually check and critically observe. It means to determine the current status and to assess whether or not required or expected performance levels are actually being achieved. ',
            'l_id'=>16,
            'sub_layer_id'=>1,
            'icon'=>'Monitor-01.png',
            'column_number' => 3
        ]);

        MetaObject::create([
            'name' => 'Control',
            'description' => 'The exercise of restraining or directing influence.  It includes decision-making aspects with accompanying decision logic necessary to ensure compliance.',
            'l_id'=>17,
            'sub_layer_id'=>1,
            'icon'=>'Control-01.png',
            'column_number' => 3
        ]);

        MetaObject::create([
            'name' => 'Organization',
            'description' => 'An arrangement or formation of resources that has a set of collective goals ',
            'l_id'=>18,
            'sub_layer_id'=>2,
            'icon'=>'Organization-01.png',
            'column_number' => 1
        ]);

        MetaObject::create([
            'name' => 'Enterprise Capability',
            'description' => 'A enterprise capability is an abstraction that represents the ability to perform a particular skillset i.e. organizational competencies, personal competencies, business function, processes, services, and technology. ',
            'l_id'=>19,
            'sub_layer_id'=>2,
            'icon'=>'Enterprise Capability-01.png',
            'column_number' => 1
        ]);

        MetaObject::create([
            'name' => 'Organizational Competency',
            'description' => 'An integrated and holistic set of related knowledge, skills, and abilities related to a specific set of resources (including persons and organizations) that - once combined - enables the enterprise to do something well.',
            'l_id'=>20,
            'sub_layer_id'=>2,
            'icon'=>'Organizational Competency-01.png',
            'column_number' => 1
        ]);

        MetaObject::create([
            'name' => 'Resource',
            'description' => 'A specific person, expertise, data, information, material, machine, land, capital or organization that is required to accomplish an activity or as a means to act on behalf of the enterprise to achieve a desired outcome.',
            'l_id'=>21,
            'sub_layer_id'=>2,
            'icon'=>'Resource-01.png',
            'column_number' => 1
        ]);

        MetaObject::create([
            'name' => 'Actor',
            'description' => 'Any person, organization, or system that many be assigned one or more roles. Actors may be internal or external to an organization.',
            'l_id'=>22,
            'sub_layer_id'=>2,
            'icon'=>'Actor-01.png',
            'column_number' => 1
        ]);

        MetaObject::create([
            'name' => 'Role',
            'description' => 'A part that something or someone has the rights, rules, competencies, and capabilities to perform. A resource and/ or actor may have a number of roles i.e. process role, service role or application role and many actors may be assigned the same role.',
            'l_id'=>23,
            'sub_layer_id'=>2,
            'icon'=>'Role-01.png',
            'column_number' => 3
        ]);

        MetaObject::create([
            'name' => 'Organizational Function',
            'description' => 'A cluster of tasks performing a specific class of jobs.',
            'l_id'=>24,
            'sub_layer_id'=>2,
            'icon'=>'Organizational Function-01.png',
            'column_number' => 2
        ]);

        MetaObject::create([
            'name' => 'Business Object',
            'description' => 'A real world thing which relate to the enterprise’s means to act.',
            'l_id'=>25,
            'sub_layer_id'=>2,
            'icon'=>'Business Object-01.png',
            'column_number' => 2
        ]);

        MetaObject::create([
            'name' => 'Product',
            'description' => 'A result and output generated by the enterprise. It has a combination of tangible and intangible attributes (i.e.. features, functions, usage).',
            'l_id'=>26,
            'sub_layer_id'=>2,
            'icon'=>'Product-01.png',
            'column_number' => 2
        ]);

        MetaObject::create([
            'name' => 'Contract',
            'description' => 'An agreement between two or more parties that establishes conditions for interaction.',
            'l_id'=>27,
            'sub_layer_id'=>2,
            'icon'=>'Contract-01.png',
            'column_number' => 2
        ]);

        MetaObject::create([
            'name' => 'Business Rule',
            'description' => 'A statement that defines or constrains some aspect of behaviour within the enterprise and always resolves to either true or false.',
            'l_id'=>28,
            'sub_layer_id'=>2,
            'icon'=>'Organizational Rule-01.png',
            'column_number' => 2
        ]);

        MetaObject::create([
            'name' => 'Business Compliance',
            'description' => 'The process or tools for verifying adherence to rules and decisions.',
            'l_id'=>29,
            'sub_layer_id'=>2,
            'icon'=>'Business Compliance-01.png',
            'column_number' => 2
        ]);

        MetaObject::create([
            'name' => 'Location',
            'description' => 'A point, facility, place or geographic position that may be referred to physically or logically.',
            'l_id'=>30,
            'sub_layer_id'=>2,
            'icon'=>'Location-01.png',
            'column_number' => 2
        ]);

        MetaObject::create([
            'name' => 'Business Channel',
            'description' => 'A means of access or otherwise interacting within an enterprise or between an enterprise and its external partners (i.e.. customers, vendors, suppliers, etc.).',
            'l_id'=>31,
            'sub_layer_id'=>2,
            'icon'=>'Business Channel-01.png',
            'column_number' => 3
        ]);

        MetaObject::create([
            'name' => 'Business Media',
            'description' => 'The material or matter used to store information (i.e. printed page, digital tape, CD, disk as well as non-volatile storage, screen, or memory).',
            'l_id'=>32,
            'sub_layer_id'=>2,
            'icon'=>'Business Media-01.png',
            'column_number' => 3
        ]);

        MetaObject::create([
            'name' => 'Business Workflow',
            'description' => 'A flow, stream, sequence, course, succession, series or progression of as well as order for the movement of information or material from one enterprise function, service or activity (work site) to another.',
            'l_id'=>33,
            'sub_layer_id'=>2,
            'icon'=>'Business Workflow-01.png',
            'column_number' => 3
        ]);

        MetaObject::create([
            'name' => 'Service Construct (setup and delivery)',
            'description' => 'The setup and arrangement which creates, organizes, and delivers business services.',
            'l_id'=>34,
            'sub_layer_id'=>3,
            'icon'=>'Service Construct-01.png',
            'column_number' => 1
        ]);

        MetaObject::create([
            'name' => 'Business Service',
            'description' => 'The externally visible [logical] deed or effort performed to satisfy a need or to fulfill a demand that is meaningful to the [business] environment.',
            'l_id'=>35,
            'sub_layer_id'=>3,
            'icon'=>'Business Service-01.png',
            'column_number' => 1
        ]);

        MetaObject::create([
            'name' => 'Service Flow  (output and input)',
            'description' => 'A set of one or more service input or output states where each service state defines a step in the service flow that - when entered - executes a certain behaviour.',
            'l_id'=>36,
            'sub_layer_id'=>3,
            'icon'=>'Service Flow-01.png',
            'column_number' => 2
        ]);

        MetaObject::create([
            'name' => 'Service Rule',
            'description' => 'A statement that defines or constrains some aspect of the creation of value within the enterprise.',
            'l_id'=>37,
            'sub_layer_id'=>3,
            'icon'=>'Service Rule-01.png',
            'column_number' => 2
        ]);

        MetaObject::create([
            'name' => 'Service Channel',
            'description' => 'A logical or physical communication path used to requisition provision or deliver outputs to or by business services.',
            'l_id'=>38,
            'sub_layer_id'=>3,
            'icon'=>'Service Channel-01.png',
            'column_number' => 3
        ]);

        MetaObject::create([
            'name' => 'Process',
            'description' => 'A set of structured activities or tasks with logical behaviour that produce a specific service or product.',
            'l_id'=>39,
            'sub_layer_id'=>4,
            'icon'=>'Business Process-01.png',
            'column_number' => 1
        ]);

        MetaObject::create([
            'name' => 'Event',
            'description' => 'Something that happens, this may include a planned occasion or a state change that recognizes the triggering or termination of processing.',
            'l_id'=>40,
            'sub_layer_id'=>4,
            'icon'=>'Event-01.png',
            'column_number' => 1
        ]);

        MetaObject::create([
            'name' => 'Gateway',
            'description' => 'Determines forking and merging of paths depending on the conditions expressed.',
            'l_id'=>41,
            'sub_layer_id'=>4,
            'icon'=>'Gateway-01.png',
            'column_number' => 2
        ]);

        MetaObject::create([
            'name' => 'Process Flow (incl. input and output)',
            'description' => 'A flow, stream, sequence, course, succession, series or progression that are based on the process input or output states where each process input or output defines the process flow that together executes a behaviour.',
            'l_id'=>42,
            'sub_layer_id'=>4,
            'icon'=>'Process Flow-01.png',
            'column_number' => 2
        ]);

        MetaObject::create([
            'name' => 'Process Rule',
            'description' => 'A statement that defines or constrains some aspect of work and always resolves to either true or false.',
            'l_id'=>43,
            'sub_layer_id'=>4,
            'icon'=>'Process Rule-01.png',
            'column_number' => 3
        ]);

        MetaObject::create([
            'name' => 'Application Component',
            'description' => 'An encapsulation of application functionality that is independent of a particular implementation.',
            'l_id'=>44,
            'sub_layer_id'=>5,
            'icon'=>'Application Component-01.png',
            'column_number' => 1
        ]);

        MetaObject::create([
            'name' => 'Application Module',
            'description' => 'A single executable part - which is part of a larger application - that provides identifiable functions and exists within a specific application component.',
            'l_id'=>45,
            'sub_layer_id'=>5,
            'icon'=>'Application Module-01.png',
            'column_number' => 1
        ]);

        MetaObject::create([
            'name' => 'Application Feature',
            'description' => 'A notable property or characteristic of an application that can include a trait or design constraint.',
            'l_id'=>46,
            'sub_layer_id'=>5,
            'icon'=>'Application Feature-01.png',
            'column_number' => 1
        ]);

        MetaObject::create([
            'name' => 'Application Function',
            'description' => 'The specification of a significant aspect of the internal behaviour of the application which acts as a broader description of a set of application features.',
            'l_id'=>47,
            'sub_layer_id'=>5,
            'icon'=>'Application Function-01.png',
            'column_number' => 1
        ]);

        MetaObject::create([
            'name' => 'Application Task',
            'description' => 'The automated behaviour of a process activity performed by an application.',
            'l_id'=>48,
            'sub_layer_id'=>5,
            'icon'=>'Application Task-01.png',
            'column_number' => 1
        ]);

        MetaObject::create([
            'name' => 'Application Service',
            'description' => 'An externally visible unit of functionality, provided by one or more components, exposed through well-defined interfaces, and meaningful to the environment.',
            'l_id'=>49,
            'sub_layer_id'=>5,
            'icon'=>'Application Service-01.png',
            'column_number' => 3
        ]);

        MetaObject::create([
            'name' => 'Information Object',
            'description' => 'Information about real world objects that can be in any medium or form.',
            'l_id'=>50,
            'sub_layer_id'=>5,
            'icon'=>'Information Object-01.png',
            'column_number' => 3
        ]);

        MetaObject::create([
            'name' => 'Application/System Flow',
            'description' => 'The specification of the sequence in which two application tasks processes, or an application task and an application event or gateway are executed, one of which provides an output which is an input to the other.',
            'l_id'=>51,
            'sub_layer_id'=>5,
            'icon'=>'Application & System Flow-01.png',
            'column_number' => 2
        ]);

        MetaObject::create([
            'name' => 'System Measurement',
            'description' => 'Measures that are defined and implementable within an application.',
            'l_id'=>52,
            'sub_layer_id'=>5,
            'icon'=>'System Measurement-01.png',
            'column_number' => 2
        ]);

        MetaObject::create([
            'name' => 'Application/System Report',
            'description' => 'Reports that are defined and implementable or implemented within or by an application.',
            'l_id'=>53,
            'sub_layer_id'=>5,
            'icon'=>'Application & System Report-01.png',
            'column_number' => 2
        ]);

        MetaObject::create([
            'name' => 'Application/System',
            'description' => 'A collection of software adding capability to the enterprise through its ability to enable work.',
            'l_id'=>54,
            'sub_layer_id'=>5,
            'icon'=>'Application-System-01.png',
            'column_number' => 2
        ]);

        MetaObject::create([
            'name' => 'Application Role',
            'description' => 'A part recognized within an application, providing behaviour to automate or enable some parts of a business function, service or process task.',
            'l_id'=>55,
            'sub_layer_id'=>5,
            'icon'=>'Application Role-01.png',
            'column_number' => 2
        ]);

        MetaObject::create([
            'name' => 'Application Rule',
            'description' => 'A business rule that is implemented within and executed by an application.',
            'l_id'=>56,
            'sub_layer_id'=>5,
            'icon'=>'Application Rule-01.png',
            'column_number' => 3
        ]);

        MetaObject::create([
            'name' => 'Application Compliance (incl. Security)',
            'description' => 'Behaviour or ability within an application whereby it can certify the integrity of application rules.',
            'l_id'=>57,
            'sub_layer_id'=>5,
            'icon'=>'Application Compliance-01.png',
            'column_number' => 3
        ]);

        MetaObject::create([
            'name' => 'Application Media',
            'description' => 'Material or matter used by an application as the source or method of accepting or providing inputs and outputs.',
            'l_id'=>58,
            'sub_layer_id'=>5,
            'icon'=>'Application Media-01.png',
            'column_number' => 3
        ]);

        MetaObject::create([
            'name' => 'Application Channel',
            'description' => 'A physical communication path used by one or more applications to requisition, provision, or deliver outputs.',
            'l_id'=>59,
            'sub_layer_id'=>5,
            'icon'=>'Application Channel-01.png',
            'column_number' => 3
        ]);

        MetaObject::create([
            'name' => 'Data Component',
            'description' => 'A cohesive collection of data that is part of an application.',
            'l_id'=>60,
            'sub_layer_id'=>6,
            'icon'=>'Data Component-01.png',
            'column_number' => 1
        ]);

        MetaObject::create([
            'name' => 'Data Object',
            'description' => 'A logical cluster of all sets of related data representing the data object view of a business or information object.',
            'l_id'=>61,
            'sub_layer_id'=>6,
            'icon'=>'Data Object-01.png',
            'column_number' => 1
        ]);

        MetaObject::create([
            'name' => 'Data Entity',
            'description' => 'An encapsulation of data where logical data entities are a specification of the organization of information to store data as a physical persistence structure e.g. data tied to applications, repositories, and services.',
            'l_id'=>62,
            'sub_layer_id'=>6,
            'icon'=>'Data Entity-01.png',
            'column_number' => 1
        ]);

        MetaObject::create([
            'name' => 'Data Table',
            'description' => 'A physical specification of the means of arranging data in rows and columns while being stored in a physical persistence structure e.g. data tied to applications, repositories, and services.',
            'l_id'=>63,
            'sub_layer_id'=>6,
            'icon'=>'Data Table-01.png',
            'column_number' => 1
        ]);

        MetaObject::create([
            'name' => 'Data Service',
            'description' => 'A standardized and uniform way of accessing information in a form that is useful to enterprise applications without requiring knowledge of its physical persistence structure.',
            'l_id'=>64,
            'sub_layer_id'=>6,
            'icon'=>'Data Service-01.png',
            'column_number' => 2
        ]);

        MetaObject::create([
            'name' => 'Data Flow',
            'description' => 'The specification of the sequence in which data moves from one state to another.',
            'l_id'=>65,
            'sub_layer_id'=>6,
            'icon'=>'Data Flow-01.png',
            'column_number' => 3
        ]);

        MetaObject::create([
            'name' => 'Data Rule',
            'description' => 'Criteria used in the process of determining or verifying values of data or generalizing certain features of data.',
            'l_id'=>66,
            'sub_layer_id'=>6,
            'icon'=>'Data Rule-01.png',
            'column_number' => 3
        ]);

        MetaObject::create([
            'name' => 'Data Compliance (incl. Security)',
            'description' => 'The means of adhering to and verifying adherence to policies and decisions about the data.',
            'l_id'=>67,
            'sub_layer_id'=>6,
            'icon'=>'Data Compliance-01.png',
            'column_number' => 2
        ]);

        MetaObject::create([
            'name' => 'Data Media',
            'description' => 'The matter or material used to store physical persistent data.',
            'l_id'=>68,
            'sub_layer_id'=>6,
            'icon'=>'Data Media-01.png',
            'column_number' => 2
        ]);

        MetaObject::create([
            'name' => 'Data Channel',
            'description' => 'A physical communication path used to requisition, provision or deliver data.',
            'l_id'=>69,
            'sub_layer_id'=>6,
            'icon'=>'Data Channel-01.png',
            'column_number' => 2
        ]);

        MetaObject::create([
            'name' => 'Platform Component',
            'description' => 'An abstract description of the features of the existing environment that the application software is expected to have to allow it to execute.',
            'l_id'=>70,
            'sub_layer_id'=>7,
            'icon'=>'Platform Component-01.png',
            'column_number' => 1
        ]);


        MetaObject::create([
            'name' => 'Platform Device',
            'description' => 'A set of platform components configured to act as a modular part of a platform.',
            'l_id'=>71,
            'sub_layer_id'=>7,
            'icon'=>'Platform Device-01.png',
            'column_number' => 1
        ]);

        MetaObject::create([
            'name' => 'Platform Function',
            'description' => 'The specification of a significant job and/or task of the internal behaviour of the platform.',
            'l_id'=>72,
            'sub_layer_id'=>7,
            'icon'=>'Platform Function-01.png',
            'column_number' => 1
        ]);

        MetaObject::create([
            'name' => 'Platform Service',
            'description' => 'A technical delivery task required to provide platform enablement mechanisms to support the delivery of one or more parts of an application.',
            'l_id'=>73,
            'sub_layer_id'=>7,
            'icon'=>'Platform Service-01.png',
            'column_number' => 2
        ]);

        MetaObject::create([
            'name' => 'Platform Rule',
            'description' => 'Criteria used in the process of determining the behaviour of the platform.',
            'l_id'=>74,
            'sub_layer_id'=>7,
            'icon'=>'Platform Rule-01.png',
            'column_number' => 2
        ]);

        MetaObject::create([
            'name' => 'Platform Compliance (incl. Security)',
            'description' => 'The means of adhering to and verifying adherence to policies and decisions about the platform.',
            'l_id'=>75,
            'sub_layer_id'=>7,
            'icon'=>'Platform Compliance-01.png',
            'column_number' => 2
        ]);

        MetaObject::create([
            'name' => 'Platform Media',
            'description' => 'The matter or material provided by a platform as the source or method for storing data.',
            'l_id'=>76,
            'sub_layer_id'=>7,
            'icon'=>'Platform Media-01.png',
            'column_number' => 3
        ]);

        MetaObject::create([
            'name' => 'Platform Channel',
            'description' => 'A physical path used by a platform to host an application software.',
            'l_id'=>77,
            'sub_layer_id'=>7,
            'icon'=>'Platform Channel-01.png',
            'column_number' => 3
        ]);

        MetaObject::create([
            'name' => 'Infrastructure Component',
            'description' => 'An abstract description of the features of the existing environment that the platform requires to operate.',
            'l_id'=>78,
            'sub_layer_id'=>8,
            'icon'=>'Infrastructure Component-01.png',
            'column_number' => 1
        ]);

        MetaObject::create([
            'name' => 'Infrastructure Device',
            'description' => 'A set of infrastructure components configured to act as a modular part of the infrastructure.',
            'l_id'=>79,
            'sub_layer_id'=>8,
            'icon'=>'Infrastructure Device-01.png',
            'column_number' => 1
        ]);

        MetaObject::create([
            'name' => 'Infrastructure Function',
            'description' => 'The specification of a significant aspect of the internal behaviour of the infrastructure which acts as a broader description of a set of infrastructure features.',
            'l_id'=>80,
            'sub_layer_id'=>8,
            'icon'=>'Infrastructure Function-01.png',
            'column_number' => 1
        ]);

        MetaObject::create([
            'name' => 'Infrastructure Feature',
            'description' => 'A notable property or characteristic of the infrastructure that can include a trait or design constraint.',
            'l_id'=>81,
            'sub_layer_id'=>8,
            'icon'=>'Infrastructure Feature-01.png',
            'column_number' => 2
        ]);

        MetaObject::create([
            'name' => 'Infrastructure Service',
            'description' => 'A technical delivery task required to provide infrastructure enablement mechanisms to support the delivery of one or more parts of a platform.',
            'l_id'=>82,
            'sub_layer_id'=>8,
            'icon'=>'Infrastructure Service-01.png',
            'column_number' => 2
        ]);

        MetaObject::create([
            'name' => 'Infrastructure Rule',
            'description' => 'Criteria used in the process of determining the behaviour of the infrastructure.',
            'l_id'=>83,
            'sub_layer_id'=>8,
            'icon'=>'Infrastructure Rule-01.png',
            'column_number' => 3
        ]);

        MetaObject::create([
            'name' => 'Infrastructure Compliance (incl. Security)',
            'description' => 'The means of adhering to and verifying adherence to policies and decisions about the infrastructure.',
            'l_id'=>84,
            'sub_layer_id'=>8,
            'icon'=>'Infrastructure Compliance-01.png',
            'column_number' => 3
        ]);

        MetaObject::create([
            'name' => 'Infrastructure Media',
            'description' => 'The matter or material provided by an infrastructure as the source or method for transmitting data.',
            'l_id'=>85,
            'sub_layer_id'=>8,
            'icon'=>'Infrastructure Media-01.png',
            'column_number' => 3
        ]);

        MetaObject::create([
            'name' => 'Infrastructure Channel',
            'description' => 'A physical communication path used by an infrastructure component to provide the resources needed by a platform.',
            'l_id'=>86,
            'sub_layer_id'=>8,
            'icon'=>'Infrastructure Channel-01.png',
            'column_number' => 2
        ]);

    }
}
