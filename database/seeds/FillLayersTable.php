<?php

use Illuminate\Database\Seeder;
use LeadApi\Layer as Layer;

class FillLayersTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        //Truncate la_layers table
        DB::table('la_layers')->truncate();
        Layer::create([
            'name' => 'Business',
            'description' => 'Contains the  business objects needed to capture and describe the nature, form, and relationships of the business.',
            'color'=>'#0b98d6'
        ]);

        Layer::create([
            'name' => 'Information',
            'description' => 'Contains the objects used to describe the structure and behaviour of major software systems and how these objects interact with one other both within the layer and across the enterprise.',
            'color'=>'#f7b206'
        ]);

        Layer::create([
            'name' => 'Technology',
            'description' => 'Contains the objects used to describe the  structure and connections of the enabling technology of the software applications and how these objects interact with one other both within the layer and across the enterprise',
            'color'=> '#a0281e'
        ]);
        //Enable checking Foreign key
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
