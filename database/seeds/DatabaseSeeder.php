<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        //Disable checking Foreign key
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->call(FillLayersTable::class);
        $this->call(FillSubLayersTable::class);
        $this->call(FillMetaObjectsTable::class);
        $this->call(FillMetaObjectsTable::class);

        //Enable checking Foreign key
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        Model::reguard();
    }
}
