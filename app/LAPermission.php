<?php

namespace LeadApi;

use Illuminate\Database\Eloquent\Model;

class LAPermission extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'la_permissions';


}
