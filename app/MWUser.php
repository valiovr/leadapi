<?php

namespace LeadApi;

use Illuminate\Database\Eloquent\Model;

class MWUser extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mw_user';
}
