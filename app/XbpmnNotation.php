<?php

namespace LeadApi;

use Illuminate\Database\Eloquent\Model;

class XbpmnNotation extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'la_xbpmn_notations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','description','notation','sub_layer_id'];

    /**
     * Get X-BPMN Notation Sub Layer.
     */
    public function sublayer()
    {
        return $this->belongsTo('LeadApi\SubLayer','sub_layer_id');
    }

    /**
     * Get X-BPMN Notation Object Category.
     */
    public function object_category()
    {
        return $this->belongsTo('LeadApi\ObjectCategory');
    }
}
