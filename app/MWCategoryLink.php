<?php

namespace LeadApi;

use Illuminate\Database\Eloquent\Model;

class MWCategoryLink extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mw_categorylinks';

    /**
     * Get the post that owns the comment.
     */
    public function category()
    {
        return $this->belongsTo('LeadApi\MWCategory', 'cl_to','cat_title');
    }
}
