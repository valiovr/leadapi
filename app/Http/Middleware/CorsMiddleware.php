<?php

namespace LeadApi\Http\Middleware;

use Closure;
use Config;

class CorsMiddleware
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $corsEnabledUrl ='';
        if($request->header('origin')==Config::get('api.cors_url')){
            $corsEnabledUrl = Config::get('api.cors_url', '');
        }else{
            $corsEnabledUrl = Config::get('api.cors_admin_url', '');
        }
        header("Access-Control-Allow-Origin: ".$corsEnabledUrl);
        header('Access-Control-Allow-Methods: GET, POST, OPTIONS, DELETE, STORE, PATCH');
        header('Access-Control-Allow-Headers: Origin, Content-Type, Accept, Authorization, X-Request-With, lead-api, accept');
        header('Access-Control-Allow-Credentials: true');

        return $next($request);
    }
}
