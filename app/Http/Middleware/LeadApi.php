<?php

namespace LeadApi\Http\Middleware;

use Closure;

class LeadApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $mw_api_key = $request->header('lead-api');
        $api_key = config('api.api_key');
        $client_ip_arr = explode(',', config('api.client_ip_list'));
        if($request->header('origin')==NULL){
            //check API key and Client IP
            if (($mw_api_key != $api_key) || (!in_array($request->ip(), $client_ip_arr))) {
                return array('error' => 'Unauthorized.');
            }
        }else{
            if (($mw_api_key != $api_key) || ($request->header('origin') != config('api.cors_url') && $request->header('origin') != config('api.cors_admin_url'))) {
                return array('error' => 'Unauthorized.');
            }
        }

        return $next($request);
    }
}
