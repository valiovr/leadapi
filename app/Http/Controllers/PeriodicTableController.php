<?php

namespace LeadApi\Http\Controllers;

use Illuminate\Http\Request;

use LeadApi\Http\Requests;
use LeadApi\Http\Controllers\Controller;
use LeadApi\SubLayer;


class PeriodicTableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return SubLayer::with('meta_objects')->get();
    }


}
