<?php

namespace LeadApi\Http\Controllers;

use Illuminate\Http\Request;

use LeadApi\Http\Requests;
use LeadApi\Http\Controllers\Controller;
use LeadApi\SubLayer;
use LeadApi\Layer as Layer;
use LeadApi\MetaObject;
use LeadApi\RelationType;
use LeadApi\ObjectRelation;
use LeadApi\UserObject;
use LeadApi\UserObjectChange;
use LeadApi\MWPage;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $pages = MWPAge::with('category_links','category_links.category')->get();
        echo '<pre>';
        var_dump($pages->toArray()); die;
//        $user_meta_object = UserObject::where('id','=',1)->with('stereotype','type','subtype','meta_object','user_object_changes')->get();
//       // $user_meta_object = UserObjectChange::where('id','=',1)->with('stereotype','type','subtype','meta_object','user_object')->get();
//        dd($user_meta_object->toArray());
        die('imported');

        $meta_obj_rel_json = file_get_contents('../meta_rel_obj.php.html');
        $meta_obj_rel_arr = json_decode($meta_obj_rel_json);

        $rel_type_arr = array();
     //  echo '<pre>';
    //        print_r($meta_obj_rel_arr);
//        foreach($meta_obj_rel_arr as $meta_arr){
//            foreach($meta_arr as $meta_key=>$meta_rel_arr){
//                if(is_object($meta_rel_arr)){
//        //create array with  Relation_types
//        //                    if($meta_rel_arr->relation_name != ''){
//        //                        $rel_type_arr[] = $meta_rel_arr->relation_name;
//        //                    }
//
//
//                    $object_relation = new ObjectRelation();
//                    $object_relation->object_id = $meta_rel_arr->obj_id_vert;
//                    if($meta_rel_arr->relation_name ==''){
//                        $object_relation->relation_type_id = NULL;
//                    }else{
//                        $rel_type = RelationType::where('name','=',$meta_rel_arr->relation_name)->first();
//                        $object_relation->relation_type_id =$rel_type->id;
//                    }
//                    $object_relation->related_object_id = $meta_rel_arr->obj_id_hor;
//
//                    $object_relation->save();
//
//                   echo 'Inserted relation: <i>'.$meta_rel_arr->obj_name_vert.'</i> rel_type: <b>'.$meta_rel_arr->relation_name. '</b> related to: <i>'.$meta_rel_arr->obj_name_hor.'</i><br />';
//
//                }
//            }
//
//        }

   //     $result = array_unique($rel_type_arr);
       // sort($result);
      //  print_r(count($result));
      //  echo '<br />';
    //    print_r($result);


//        foreach($result as $rel_type_el){
//            $rel_type = new RelationType;
//            $rel_type->name = $rel_type_el;
//            $rel_type->save();
//            echo'<b>Inserted: </b>'.$rel_type_el.'<br />';
//        }

//       $layersData = Layer::with('sublayers','sublayers.meta_objects')->get();
//        return $layersData;

//        $subLayersData = SubLayer::with('layer','meta_objects')->get();
//        return $subLayersData;

//        $metaObjectsData = MetaObject::with('sublayer','sublayer.layer')->get();
//        return $metaObjectsData;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
