<?php

namespace LeadApi\Http\Controllers;

use LeadApi\Http\Requests;
use LeadApi\Http\Requests\UserObjectRequest;
use LeadApi\Layer;
use LeadApi\MetaObject;
use LeadApi\UserObject;

class MetaObjectsController extends Controller {
    /**
     * Get Layers data with Sublayers and their MetaObjects.
     *
     * @return array
     */
    public function index() {
        return Layer::with('sublayers', 'sublayers.meta_objects', 'sublayers.meta_objects.user_object')->get();
    }

    /**
     * Get Meta Object by ID
     * @param $id
     * @return mixed
     */
    public function getMetaObjectById($id) {
        return MetaObject::where('id', '=', $id)
            ->with(
                ['stereotype' => function ($query) {
                    $query->where('parent_id', '=', 0);
                },
                    'stereotype.children',
                    'stereotype.children.children'
                ]
            )->first();
    }

    public function getDescriptions($id) {
        return Layer::with(['sublayers',
            'sublayers.meta_objects',
            'sublayers.meta_objects.user_object' => function ($query) use ($id) {
                $query->where('user_id', '=', $id);
            }
        ])
            ->get();
    }

    public function createNewMetaObjectInstance(UserObjectRequest $request) {
        $requestData = $request->all();

        //if stereotype, type or subtype is empty set value to NULL
        if ($requestData['subtype_id'] == '') {
            $requestData['subtype_id'] = NULL;
        }
        if ($requestData['type_id'] == '') {
            $requestData['type_id'] = NULL;
        }
        if ($requestData['stereotype_id'] == '') {
            $requestData['stereotype_id'] = NULL;
        }
        return UserObject::create($requestData);
    }

    /**
     * Get Needed Data to be displayed on Meta Object Internal Page
     * @param string $title
     * @return object mixed
     */
    public function getMetaObjectInternalData($title) {
        return MetaObject::where('name', '=', urldecode(str_replace('___','/',$title)))
            ->select('id', 'name', 'description', 'icon', 'sub_layer_id')
            ->with(array('sublayer' => function ($q) {
                    $q->select('id', 'name', 'layer_id');
                },
                    'sublayer.layer' => function ($q) {
                        $q->select('id', 'name');
                    },
                    'object_relations_inverse' => function ($q) {
                        $q->select('id', 'object_id', 'relation_type_id', 'related_object_id');
                    },
                    'object_relations_inverse.meta_object' => function ($q) {
                        $q->select('id', 'name');
                    },
                    'object_relations_inverse.related_object' => function ($q) {
                        $q->select('id', 'name');
                    },
                    'object_relations_inverse.relation_type' => function ($q) {
                        $q->select('id', 'name');
                    }
                )
            )
            ->first();


    }
}
