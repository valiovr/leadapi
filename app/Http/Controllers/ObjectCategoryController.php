<?php

namespace LeadApi\Http\Controllers;

use LeadApi\Http\Requests;
use LeadApi\ObjectCategory;
use LeadApi\XbpmnNotation;

class ObjectCategoryController extends Controller {
    
    /**
     * Get Object Category Memebers
     * @param $title
     * @return array
     */
    public function getObjectCategoryMembers($title) {

        $objectCategory= ObjectCategory::where('name', '=', urldecode($title))
            ->select('id','object_class_id')
            ->with(array('object_class'=>function ($q) {
                $q->select('id', 'name');
            }))
            ->first();
        $buildModelName=substr(str_replace(' ','',ucwords(str_replace('-','',strtolower($objectCategory['object_class']['name'])))),0,-1);
        $model = '\LeadApi\\'.$buildModelName;

        if ($objectCategory != null) {
            return $model::where('object_category_id', '=', $objectCategory['id'])
                ->select('id', 'name', 'object_category_id')
                ->orderBy('name', 'asc')
                ->get();
        }
        //XbpmnNotation

        return array();
    }
}
