<?php

namespace LeadApi\Http\Controllers;

use LeadApi\Http\Requests;
use LeadApi\MWPage;
use LeadApi\Permission;

class PermissionsController extends Controller {


    public function checkPermissions($object_id, $user_id) {
        $has_permission = false;
        //Get Current Page Data
        $page = MWPage::where('page_id', '=', $object_id)->with('category_links', 'category_links.category')->first();

        //Get namespace and category id if current page
        $namespace = $page['page_namespace'];
        $categoryId = $page['category_links']['category']['cat_id'];

        //Check Namespace Permissions
        $namespacePermissions = Permission::where('object_type', '=', 'namespace')
            ->where('object_id', '=', $namespace)
            ->where('user_id', '=', $user_id)
            ->first();

        if ($namespacePermissions != null) {
            //Check permission here
            if ($namespacePermissions['access_allowed'] == 1) {
                $has_permission = true;
            } else {
                $has_permission = false;
            }
        }

        //Check Category Permissions
        $categoryPermissions = Permission::where('object_type', '=', 'category')
            ->where('object_id', '=', $categoryId)
            ->where('user_id', '=', $user_id)
            ->first();

        if ($categoryPermissions != null) {
            //Check permission
            if ($categoryPermissions['access_allowed'] == 1) {
                $has_permission = true;
            } else {
                $has_permission = false;
            }
        }

        //Check Category Permissions
        $pagePermissions = Permission::where('object_type', '=', 'page')
            ->where('object_id', '=', $object_id)
            ->where('user_id', '=', $user_id)
            ->first();

        if ($pagePermissions != null) {
            //Check permission
            if ($pagePermissions['access_allowed'] == 1) {
                $has_permission = true;
            } else {
                $has_permission = false;
            }
        }

        return response()->json(['has_permissions' => $has_permission]);
    }
}
