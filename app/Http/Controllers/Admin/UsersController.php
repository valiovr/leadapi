<?php

namespace LeadApi\Http\Controllers\Admin;

use Illuminate\Http\Request;

use LeadApi\Http\Requests;
use LeadApi\Http\Controllers\Controller;
use LeadApi\MWUser;

class UsersController extends Controller
{
    public function getAllUsers($type='user_id',$order='asc'){
      return MWUser::orderBy($type, $order)
          ->get();
    }

    public function sortUsers($type,$order){
        return MWUser::orderBy($type, $order)
            ->get();
    }

    public function getUserData($user_id){
        return MWUser::where('user_id', $user_id)->get();
    }
}
