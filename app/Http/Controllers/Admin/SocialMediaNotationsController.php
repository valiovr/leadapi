<?php

namespace LeadApi\Http\Controllers\Admin;

use DB;
use LeadApi\Http\Controllers\Controller;
use LeadApi\Http\Requests;
use LeadApi\Http\Requests\SocialMediaNotationRequest;
use LeadApi\ObjectCategory;
use LeadApi\SocialMediaNotation;

class SocialMediaNotationsController extends Controller {
    /**
     * Get All Social Media Notations
     * @return mixed
     */
    public function getSocialMediaNotations() {
        return SocialMediaNotation::select('id', 'name', 'notation', 'sub_layer_id', 'object_category_id')
            ->with(array('sublayer' => function ($q) {
                    $q->select('id', 'name', 'color', 'layer_id');
                },
                    'sublayer.layer' => function ($q) {
                        $q->select('id', 'name', 'color');
                    },
                    'object_category' => function ($q) {
                        $q->select('id', 'name');
                    }
                )
            )
            ->get();
    }

    /**
     * Get Social Media Notations Object Categories
     * @return mixed
     */
    public function getSocialMediaNotationsCategories() {
        return ObjectCategory::select('id', 'name', 'object_class_id')
            ->where('object_class_id', '=', 3)
            ->orderBy('name', 'asc')
            ->get();
    }

    /**
     * Get Social Media Notation By ID
     *
     * @param Integer $id
     * @return mixed
     */
    public function getSocialMediaNotationById($id) {
        return SocialMediaNotation::where('id', '=', $id)->select('id', 'name', 'description', 'notation', 'object_category_id')->first();
    }

    /**
     * Save (add/edit) Social Media Notation
     * @param SocialMediaNotationRequest $request
     * @return array
     */
    public function saveSocialMediaNotation(SocialMediaNotationRequest $request) {
        $requestData = $request->all();

        $image = \Input::file('file');
        $destinationPath = \Config::get('api.image_upload_dir') . '/social_media_notations/';

        if ($image != null) {
            if ($image->isValid()) {
                $extension = $image->getClientOriginalExtension(); // getting image extension
                $fileName = md5(time()) . '.' . $extension; // renameing image

                $image->move($destinationPath, $fileName);
            } else {
                return array('error' => 'social_media_notation_upload_error');
            }
        }

        if (!isset($requestData['id'])) {
            $socialMediaNotation = new SocialMediaNotation();
            //Hardcoded value Because Social Media Notation cannot be moved to another Sub Layer
            $socialMediaNotation->sub_layer_id = 5;
        } else {
            $socialMediaNotation = SocialMediaNotation::where('id', '=', $requestData['id'])->first();
        }

        $socialMediaNotation->name = $requestData['name'];
        if (isset($requestData['description'])) {
            $socialMediaNotation->description = $requestData['description'];
        }
        $socialMediaNotation->object_category_id = $requestData['object_category_id'];
        if ($image != null) {
            $socialMediaNotation->notation = $fileName;
        }
        //Begin Transaction
        DB::beginTransaction();
        try {
            $socialMediaNotation->save();
        } catch (\Exception $e) {
            // Rollback
            DB::rollback();
            unlink($destinationPath . $fileName);
            return array('error' => 'social_media_notation_not_saved');
        }

        //Commit Transaction
        DB::commit();
        return array('success' => 'social_media_notation_saved');
    }
}
