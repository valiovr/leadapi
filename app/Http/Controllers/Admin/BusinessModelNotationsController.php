<?php

namespace LeadApi\Http\Controllers\Admin;

use DB;
use LeadApi\Http\Controllers\Controller;
use LeadApi\Http\Requests;
use LeadApi\Http\Requests\BusinessModelNotationRequest;
use LeadApi\ObjectCategory;
use LeadApi\BusinessModelNotations;

class BusinessModelNotationsController extends Controller
{
    /**
     * Get All Business Model Notations
     * @return mixed
     */

    public function getBusinessModelNotations()
    {

        return BusinessModelNotations::select('id', 'name', 'notation', 'sub_layer_id', 'object_category_id')
            ->with(array('sublayer' => function ($q) {
                    $q->select('id', 'name', 'color', 'layer_id');
                },
                    'sublayer.layer' => function ($q) {
                        $q->select('id', 'name', 'color');
                    },
                    'object_category' => function ($q) {
                        $q->select('id', 'name');
                    }
                )
            )
            ->get();
    }

    /**
     * GetBusiness Model Notations Categories
     * @return mixed
     */

    public function getBusinessModelNotationsCategories()
    {
        return ObjectCategory::select('id', 'name', 'object_class_id')
            ->where('object_class_id', '=', 2)
            ->orderBy('name', 'asc')
            ->get();
    }

    public function addBusinessModelNotation(BusinessModelNotationRequest $request)
    {
        $requestData = $request->all();

        $image = \Input::file('file');
        $destinationPath = \Config::get('api.image_upload_dir') . '/business_model_notations/';

        if ($image->isValid()) {
            $extension = $image->getClientOriginalExtension(); //getting image extensions
            $fileName = md5(time()) . '.' . $extension; // renaming image

            $image->move($destinationPath, $fileName);

            $businessMediaNotation = new BusinessModelNotations();
            $businessMediaNotation->name = $requestData['name'];
            $businessMediaNotation->description = $requestData['description'];
            $businessMediaNotation->sub_layer_id = 2;
            $businessMediaNotation->object_category_id = $requestData['object_category_id'];
            $businessMediaNotation->notation = $fileName;

            DB::beginTransaction();
            try {
                $businessMediaNotation->save();
            } catch (\Exception $e) {
                // Rollback
                DB::rollback();
                unlink($destinationPath . $fileName);
                return array('error' => 'business_model_notation_not_updated');
            }
            //Commit Transaction
            DB::commit();
            return array('success' => 'business_model_notation_updated', 'file_name' => $fileName);

        }
        return array('error' => 'business_model_notation_upload_error');

    }
    public function getBusinessModelNotationById($id){
        return BusinessModelNotations::where('id', '=', $id)->select('id', 'name', 'description', 'notation', 'object_category_id')->first();
    }

    public function editBusinessModelNotation(BusinessModelNotationRequest $request) {
        $requestData = $request->all();

        $image = \Input::file('file');
        $destinationPath = \Config::get('api.image_upload_dir') . '/business_model_notations/';

        if ($image != null) {
            if ($image->isValid()) {
                $extension = $image->getClientOriginalExtension(); // getting image extension
                $fileName = md5(time()) . '.' . $extension; // renaming image

                $image->move($destinationPath, $fileName);
            } else {
                return array('error' => 'business_model_notation_upload_error');
            }
        }

        $businessMediaNotation = BusinessModelNotations::where('id', '=', $requestData['id'])->first();
        $businessMediaNotation->name = $requestData['name'];
        if (isset($requestData['description'])) {
            $businessMediaNotation->description = $requestData['description'];
        }
        $businessMediaNotation->object_category_id = $requestData['object_category_id'];
        if ($image != null) {
            $businessMediaNotation->notation = $fileName;
        }
        //Begin Transaction
        DB::beginTransaction();
        try {
            $businessMediaNotation->save();
        } catch (\Exception $e) {
            // Rollback
            DB::rollback();
            unlink($destinationPath . $fileName);
            return array('error' => 'business_model_notation_not_updated');
        }

        //Commit Transaction
        DB::commit();
        return array('success' => 'business_model_notation_updated');

    }

}
