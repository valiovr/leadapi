<?php

namespace LeadApi\Http\Controllers\Admin;

use Illuminate\Http\Request;
use LeadApi\Http\Requests\PermissionsRequest;
use LeadApi\Http\Requests;
use LeadApi\Http\Controllers\Controller;
use LeadApi\MWCategory;
use LeadApi\LAPermission;

class CategoryController extends Controller
{
    public function index(){

    }

    public function getAllCategories(){

       return MWCategory::select('cat_id as id','cat_title')->get();
    }

    public function setPermissions(PermissionsRequest $request){
    	$userId 		 = $request->user_id;
    	$allow_namespace = $request->namespacesallow;
    	$deny_namespace  = $request->namespacesdeny;
    	$allow_category  = $request->categoryallow;
    	$deny_category	 = $request->categorydeny;
    	$allow_page		 = $request->pageallow;
    	$deny_page		 = $request->pagedeny;

    	if($allow_namespace!=NULL){
    		foreach($allow_namespace as $namespace){
    			$existCat = LAPermission::where('user_id','=', $userId)
    								 ->where('object_id','=', $namespace)
    								 ->get();
    			if($existCat->isEmpty()){
    			 $setPermission = new LAPermission();
    			 $setPermission->object_type = 'namespace';
    			 $setPermission->object_id = $namespace;
    			 $setPermission->user_id = $userId ;
    			 $setPermission->access_allowed = 1;
    			 $setPermission->save();
    			}
    			else{
    				$updatePermission = LAPermission::where('user_id','=', $userId)
    								 ->where('object_id','=', $namespace)
    								 ->first();
    				$updatePermission->access_allowed = 1;
    				$updatePermission->save();
    			}
    		} 	
    	}
    	if($deny_namespace!=NULL){
    		foreach($deny_namespace as $namespace){
    			$existCat = LAPermission::where('user_id','=', $userId)
    								 ->where('object_id','=', $namespace)
    								 ->get();
    			if($existCat->isEmpty()){
    			 $setPermission = new LAPermission();
    			 $setPermission->object_type = 'namespace';
    			 $setPermission->object_id = $namespace;
    			 $setPermission->user_id = $userId ;
    			 $setPermission->access_allowed = 0;
    			 $setPermission->save();
    			}
    			else{
    				$updatePermission = LAPermission::where('user_id','=', $userId)
    								 ->where('object_id','=', $namespace)
    								 ->first();
    				$updatePermission->access_allowed = 0;
    				$updatePermission->save();
    			}
    		} 	
    	}


    	if($allow_category!=NULL){
    		foreach($allow_category as $category){
    			$existCat = LAPermission::where('user_id','=', $userId)
    								 ->where('object_id','=', $category)
    								 ->get();
    			if($existCat->isEmpty()){
    			 $setPermission = new LAPermission();
    			 $setPermission->object_type = 'category';
    			 $setPermission->object_id = $category;
    			 $setPermission->user_id = $userId ;
    			 $setPermission->access_allowed = 1;
    			 $setPermission->save();
    			}
    			else{
    				$updatePermission = LAPermission::where('user_id','=', $userId)
    								 ->where('object_id','=', $category)
    								 ->first();
    				$updatePermission->access_allowed = 1;
    				$updatePermission->save();
    			}
    		} 	
    	}
    	if($deny_category!=NULL){
    		foreach($deny_category as $category){
    			$existCat = LAPermission::where('user_id','=', $userId)
    								 ->where('object_id','=', $category)
    								 ->get();
    			if($existCat->isEmpty()){
    			 $setPermission = new LAPermission();
    			 $setPermission->object_type = 'category';
    			 $setPermission->object_id = $category;
    			 $setPermission->user_id = $userId ;
    			 $setPermission->access_allowed = 0;
    			 $setPermission->save();
    			}
    			else{
    				$updatePermission = LAPermission::where('user_id','=', $userId)
    								 ->where('object_id','=', $category)
    								 ->first();
    				$updatePermission->access_allowed = 0;
    				$updatePermission->save();
    			}
    		} 	
    	}

    	 if($allow_page!=NULL){
    		foreach($allow_page as $page){
    			$existCat = LAPermission::where('user_id','=', $userId)
    								 ->where('object_id','=', $page)
    								 ->get();
    			if($existCat->isEmpty()){
    			 $setPermission = new LAPermission();
    			 $setPermission->object_type = 'page';
    			 $setPermission->object_id = $page;
    			 $setPermission->user_id = $userId ;
    			 $setPermission->access_allowed = 1;
    			 $setPermission->save();
    			}
    			else{
    				$updatePermission = LAPermission::where('user_id','=', $userId)
    								 ->where('object_id','=', $page)
    								 ->first();
    				$updatePermission->access_allowed = 1;
    				$updatePermission->save();
    			}
    		} 	
    	}
    	if($deny_page!=NULL){
    		foreach($deny_page as $page){
    			$existCat = LAPermission::where('user_id','=', $userId)
    								 ->where('object_id','=', $page)
    								 ->get();
    			if($existCat->isEmpty()){
    			 $setPermission = new LAPermission();
    			 $setPermission->object_type = 'page';
    			 $setPermission->object_id = $page;
    			 $setPermission->user_id = $userId ;
    			 $setPermission->access_allowed = 0;
    			 $setPermission->save();
    			}
    			else{
    				$updatePermission = LAPermission::where('user_id','=', $userId)
    								 ->where('object_id','=', $page)
    								 ->first();
    				$updatePermission->access_allowed = 0;
    				$updatePermission->save();
    			}
    		} 	
    	}


  		
    }


}
