<?php

namespace LeadApi\Http\Controllers\Admin;

use DB;
use LeadApi\Http\Controllers\Controller;
use LeadApi\Http\Requests;
use LeadApi\Http\Requests\XbpmnNotationRequest;
use LeadApi\ObjectCategory;
use LeadApi\XbpmnNotation;

class XbpmnNotationsController extends Controller {

    /**
     * Get All X-BPMN Notations
     * @return mixed
     */
    public function getXbpmnNotations() {
        return XbpmnNotation::select('id', 'name', 'notation', 'sub_layer_id', 'object_category_id')
            ->with(array('sublayer' => function ($q) {
                    $q->select('id', 'name', 'color', 'layer_id');
                },
                    'sublayer.layer' => function ($q) {
                        $q->select('id', 'name', 'color');
                    },
                    'object_category' => function ($q) {
                        $q->select('id', 'name');
                    }
                )
            )
            ->get();
    }

    /**
     * Get X-BPMN Notations Object Categories
     * @return mixed
     */
    public function getXbpmnNotationsCategories() {
        return ObjectCategory::select('id', 'name', 'object_class_id')
            ->where('object_class_id', '=', 1)
            ->orderBy('name', 'asc')
            ->get();
    }

    /**
     * Get X-BPMN Notations By ID
     *
     * @param Integer $id
     * @return mixed
     */
    public function getXbpmnNotationById($id) {
        return XbpmnNotation::where('id', '=', $id)->select('id', 'name', 'description', 'notation', 'object_category_id')->first();
    }

    /**
     * Add X-BPMN Notation to database and move uploaded icon
     *
     * @param XbpmnNotationRequest $request
     * @return array
     */
    public function addXbpmnNotation(XbpmnNotationRequest $request) {
        $requestData = $request->all();

        $image = \Input::file('file');
        $destinationPath = \Config::get('api.image_upload_dir') . '/xbpmn_notations/';

        if ($image->isValid()) {
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $fileName = md5(time()) . '.' . $extension; // renameing image

            $image->move($destinationPath, $fileName);

            $xbpmnNotation = new XbpmnNotation();
            $xbpmnNotation->name = $requestData['name'];
            if (isset($requestData['description'])) {
                $xbpmnNotation->description = $requestData['description'];
            }
            $xbpmnNotation->sub_layer_id = 4;
            $xbpmnNotation->object_category_id = $requestData['object_category_id'];
            $xbpmnNotation->notation = $fileName;
            //Begin Transaction
            DB::beginTransaction();
            try {
                $xbpmnNotation->save();
            } catch (\Exception $e) {
                // Rollback
                DB::rollback();
                unlink($destinationPath . $fileName);
                return array('error' => 'xbpmn_notation_not_added');
            }

            //Commit Transaction
            DB::commit();
            return array('success' => 'xbpmn_notation_updated', 'file_name' => $fileName);
        }

        return array('error' => 'xbpmn_notation_upload_error');
    }

    /**
     * Edit X-BPMN Notation to database and move uploaded icon
     *
     * @param XbpmnNotationRequest $request
     * @return array
     */
    public function editXbpmnNotation(XbpmnNotationRequest $request) {
        $requestData = $request->all();

        $image = \Input::file('file');
        $destinationPath = \Config::get('api.image_upload_dir') . '/xbpmn_notations/';

        if ($image != null) {
            if ($image->isValid()) {
                $extension = $image->getClientOriginalExtension(); // getting image extension
                $fileName = md5(time()) . '.' . $extension; // renameing image

                $image->move($destinationPath, $fileName);
            } else {
                return array('error' => 'xbpmn_notation_upload_error');
            }
        }

        $xbpmnNotation = XbpmnNotation::where('id', '=', $requestData['id'])->first();
        $xbpmnNotation->name = $requestData['name'];
        if (isset($requestData['description'])) {
            $xbpmnNotation->description = $requestData['description'];
        }
        $xbpmnNotation->object_category_id = $requestData['object_category_id'];
        if ($image != null) {
            $xbpmnNotation->notation = $fileName;
        }
        //Begin Transaction
        DB::beginTransaction();
        try {
            $xbpmnNotation->save();
        } catch (\Exception $e) {
            // Rollback
            DB::rollback();
            unlink($destinationPath . $fileName);
            return array('error' => 'xbpmn_notation_not_updated');
        }

        //Commit Transaction
        DB::commit();
        return array('success' => 'xbpmn_notation_updated');
    }

}
