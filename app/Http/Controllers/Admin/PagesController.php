<?php

namespace LeadApi\Http\Controllers\Admin;

use Illuminate\Http\Request;

use LeadApi\Http\Requests;
use LeadApi\Http\Controllers\Controller;
use LeadApi\MWPage;

class PagesController extends Controller
{
    public function index(){

    }

    public function getAllPages(){

        return MWPage::select('page_id as id','page_title as title')->whereNotIn('page_namespace',[-1, -2, 2, 4, 6, 8, 10 , 12, 14])->get();
    }
}
