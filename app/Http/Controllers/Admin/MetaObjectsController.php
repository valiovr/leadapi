<?php

namespace LeadApi\Http\Controllers\Admin;

use DB;
use Illuminate\Http\Request;
use LeadApi\Http\Controllers\Controller;
use LeadApi\Http\Requests;
use LeadApi\Http\Requests\MetaObjectNoImageRequest;
use LeadApi\MetaObject;

class MetaObjectsController extends Controller {
    /**
     * Get Meta Objects List
     * @return mixed
     */
    public function getMetaObjects() {
        return MetaObject::select('id', 'name', 'icon', 'sub_layer_id')
            ->with(array('sublayer' => function ($q) {
                    $q->select('id', 'name', 'color', 'layer_id');
                },
                    'sublayer.layer' => function ($q) {
                        $q->select('id', 'name', 'color');
                    }
                )
            )
            ->get();
    }

    /**
     * Get Meta Object By ID
     * @param $id
     * @return mixed
     */
    public function getMetaObjectById($id) {
        return MetaObject::where('id', '=', $id)->select('id', 'name', 'description', 'icon')->first();
    }

    /**
     * Save Meta object without change Notation
     * @param MetaObjectNoImageRequest $request
     * @return array
     */
    public function saveMetaObject(MetaObjectNoImageRequest $request) {
        $requestData = $request->all();
        $currentMetaObject = MetaObject::where('id', '=', $requestData['id'])->first();
        if (!empty($currentMetaObject)) {
            $currentMetaObject->name = $requestData['name'];
            $currentMetaObject->description = $requestData['description'];

            $newObject = $currentMetaObject->save();
            return array('success' => $newObject);
        }

        return array('error' => true);
    }

    public function saveMetaObjectWithNotation(Request $request) {
        $requestData = $request->all();
        $image = \Input::file('file');
        $destinationPath = \Config::get('api.image_upload_dir') . '/meta_objects/';

        if ($image->isValid()) {
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $fileName = md5(time()) . '.' . $extension; // renaming image

            $image->move($destinationPath, $fileName);

            $currentMetaObject = MetaObject::where('id', '=', $requestData['id'])->first();
            if (!empty($currentMetaObject)) {
                $currentMetaObject->name = $requestData['name'];
                $currentMetaObject->description = $requestData['description'];
                $currentMetaObject->icon = $fileName;
                //Begin Transaction
                DB::beginTransaction();
                try {
                    $currentMetaObject->save();
                } catch (\Exception $e) {
                    // Rollback
                    DB::rollback();
                    return array('error' => 'meta_object_not_updated');
                }
                //Commit Transaction
                DB::commit();
                return array('success' => 'meta_object_updated', 'file_name' => $fileName);
            }
        }

        return array('error' => 'meta_object_notation_upload_error');
    }
}
