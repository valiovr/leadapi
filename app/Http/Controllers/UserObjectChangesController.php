<?php

namespace LeadApi\Http\Controllers;

use DB;
use LeadApi\Http\Requests;
use LeadApi\Http\Requests\RevertUserObjectChangeRequest;
use LeadApi\UserObject;
use LeadApi\UserObjectChange;

class UserObjectChangesController extends Controller {

    /**
     * Delete User Object Change from database
     *
     * @param $id
     * @param $user_id
     * @return int
     */
    public function deleteUserObjectChange($id, $user_id) {
        $isAllowedToDelete = UserObjectChange::where('id', '=', $id)->where('user_id', '=', $user_id)->first();
        if ($isAllowedToDelete == NULL) {
            $result = 3;
        } else {
            $result = UserObjectChange::destroy($id);;
        }
        return $result;
    }

    public function revertUserObjectChange(RevertUserObjectChangeRequest $request) {
        $postData = $request->all();

        $userObjectChange = UserObjectChange::where('id', '=', $postData['id'])->where('user_id', '=', $postData['user_id'])->first();
        //User is not authorized to revert this modification
        if (!$userObjectChange) {
            return array('error' => 'not_authorized_to_revert');
        }

        $userObject = UserObject::where('id', '=', $userObjectChange->user_object_id)->where('user_id', '=', $postData['user_id'])->first();
        //Missing User Object in database
        if (!$userObject) {
            return array('error' => 'missing_user_object');
        }
        //new User Object change data build
        $newUserChange = new UserObjectChange();
        $newUserChange->name = $userObject->name;
        $newUserChange->icon = $userObject->icon;
        $newUserChange->stereotype_id = $userObject->stereotype_id;
        $newUserChange->type_id = $userObject->type_id;
        $newUserChange->subtype_id = $userObject->subtype_id;
        $newUserChange->user_id = $userObject->user_id;
        $newUserChange->meta_object_id = $userObject->meta_object_id;
        $newUserChange->user_object_id = $userObject->id;

        //Begin Transaction
        DB::beginTransaction();
        //Save User Object to User Object Changes table
        try {
           $newUserChange->save();
        } catch (\Exception $e) {
            // Rollback
            DB::rollback();
            return array('error' => 'user_object_change_not_saved');
        }
        //Revert User Object Change to User Objects table
        $userObject->name = $userObjectChange->name;
        $userObject->icon = $userObjectChange->icon;
        $userObject->stereotype_id = $userObjectChange->stereotype_id;
        $userObject->type_id = $userObjectChange->type_id;
        $userObject->subtype_id = $userObjectChange->subtype_id;
        $userObject->user_id = $userObjectChange->user_id;
        $userObject->meta_object_id = $userObjectChange->meta_object_id;
        try {
            $userObject->save();
        } catch (\Exception $e) {
            // Rollback
            DB::rollback();
            return array('error' => 'user_object_revert_not_saved');
        }
        //Commit Transaction
        DB::commit();

        return array('success' => 'success_object_modification_reverted');
    }
}
