<?php

namespace LeadApi\Http\Controllers;

use Illuminate\Http\Request;
use LeadApi\Http\Requests;
use LeadApi\Layer;
use LeadApi\ObjectRelation;
class SemanticRelationshipsController extends Controller
{
    /**
     * Get Layers data with Sublayers and their MetaObjects.
     *
     * @return array
     */
    public function index()
    {
        return Layer::with(array('sublayers' => function ($q) {
            $q->select('id', 'name', 'color', 'layer_id');
        }, 'sublayers.meta_objects' => function ($q) {
            $q->select('id', 'icon', 'sub_layer_id', 'name');
        }))->select('id', 'name', 'color')->get();


    }

    public function getObjectRelations($data)
    {
        $rez = json_decode($data, true);
        return ObjectRelation::where('object_id',$rez['selected_object'])->whereIn('related_object_id',$rez['active_meta_objects'])->whereNotNull('relation_type_id')->with(array('relation_type'=>function($q){
            $q->select('id','name');
        }))->select('id', 'object_id', 'relation_type_id', 'related_object_id')->get();

    }

}
