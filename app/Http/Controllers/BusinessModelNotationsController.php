<?php

namespace LeadApi\Http\Controllers;

use Illuminate\Http\Request;

use LeadApi\Http\Requests;
use LeadApi\Http\Controllers\Controller;
use LeadApi\BusinessModelNotations;
use LeadApi\ObjectClass;
use LeadApi\ObjectCategory;

class BusinessModelNotationsController extends Controller
{
    /**
     * Get BusinessModel Notation  Inner data
     * @param string$title
     * @return mixed
     */
    public function getBusinessModelNotationInnerData($title) {
        return BusinessModelNotations::where('name', '=', urldecode(str_replace('&#38;','&', str_replace('___','/',$title))))
            ->select('id', 'name', 'description', 'notation', 'sub_layer_id','object_category_id')->with(
                array(
                    'sublayer' => function ($q) {
                        $q->select('id', 'name', 'layer_id');
                    },
                    'sublayer.layer' => function ($q) {
                        $q->select('id', 'name');
                    },
                    'object_category' => function ($q) {
                        $q->select('id', 'name','object_class_id');
                    },
                    'object_category.object_class' => function ($q) {
                        $q->select('id', 'name');
                    }
                )
            )
            ->first();
    }

    public function getBusinessModelNotationsList($title){
        $objectClassArr = ObjectClass::where('name','=',urldecode($title))->first();
        if($objectClassArr!= null){
            $businessModelNotations = ObjectCategory::where('object_class_id','=',$objectClassArr['id'])
                ->select('id','name','object_class_id')
                ->with(array('business_model_notations' => function ($q) {
                $q->select('id', 'name','description','notation','object_category_id');
            }))->get()->toArray();

            if(!empty($businessModelNotations)){
            return array('success'=>$businessModelNotations);
            }
        }

        return array('error'=>'no_business_model_notations_exists');

    }
}
