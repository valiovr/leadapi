<?php

namespace LeadApi\Http\Controllers;

use DB;
use LeadApi\Http\Requests;
use LeadApi\Http\Requests\UserObjectModifyRequest;
use LeadApi\UserObject;
use LeadApi\UserObjectChange;

class UserObjectsController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $objs = UserObject::all();
        return response()->json($objs);
    }

    /**
     * Delete User Object from database
     * @param $id
     * @param $user_id
     * @return int
     */
    public function deleteUserObject($id, $user_id) {
        $isAllowedToDelete = UserObject::where('id', '=', $id)->where('user_id', '=', $user_id)->first();
        if ($isAllowedToDelete == NULL) {
            $result = 3;
        } else {
            $result = UserObject::destroy($id);;
        }
        return $result;
    }

    /**
     * Get User Objects for current logged user in mediawiki
     * @param $userId
     * @return mixed
     */
    public function getUserObjects($userId) {
        return UserObject::where('user_id', $userId)
            ->with([
                    'meta_object.sublayer',
                    'meta_object.stereotype' => function ($query) {
                        $query->where('parent_id', '=', 0);
                    },
                    'meta_object.stereotype.children',
                    'meta_object.stereotype.children.children',
                    'user_object_changes'
                ]
            )
            ->get();
    }

    public function getUserMyObjects($userId) {
        return UserObject::where('user_id', $userId)
        ->with([
                    'meta_object.stereotype' => function ($query) {
                         $query->select(['id','name', 'parent_id','object_id'])->where('parent_id', '=', 0);
                    },
                    'meta_object' => function ($query) {
                         $query->select(['id','name', 'sub_layer_id']);
                    },
                    'user_object_changes'
                ]
            )
            ->get();



    }

        public function getUserInstanceObjects($userId, $instanceId) {
        return UserObject::where('user_id', $userId)
            ->where('meta_object_id', $instanceId)
                    ->with([
                    'meta_object.stereotype' => function ($query) {
                         $query->select(['id','name', 'parent_id','object_id'])->where('parent_id', '=', 0);
                    },
                    'meta_object' => function ($query) {
                         $query->select(['id','name', 'sub_layer_id']);
                    },
                    'user_object_changes'
                ]
            )
            ->get();
    }



    /**
     * Modify User Object and move old version of object
     * to user object changes table
     *
     * @param UserObjectModifyRequest $request
     * @return array
     */
    public function UserObjectModify(UserObjectModifyRequest $request) {

        $requestData = $request->all();

        //get current user object
        $currentUserObject = UserObject::where('id', '=', $requestData['id'])->first();

        $userObjectChange = new UserObjectChange();

        //Prepare data for moving in changes table
        $userObjectChange->name = $currentUserObject->name;
        $userObjectChange->icon = $currentUserObject->icon;
        $userObjectChange->stereotype_id = $currentUserObject->stereotype_id;
        $userObjectChange->type_id = $currentUserObject->type_id;
        $userObjectChange->subtype_id = $currentUserObject->subtype_id;
        $userObjectChange->user_id = $currentUserObject->user_id;
        $userObjectChange->meta_object_id = $currentUserObject->meta_object_id;
        $userObjectChange->user_object_id = $currentUserObject->id;

        //Begin Transaction
        DB::beginTransaction();

        //Save User Object to User Object Changes table
        try {
            $userObjectChange->save();
        } catch (\Exception $e) {
            // Rollback
            DB::rollback();
            return array('error' => 'user_object_modification_error');
        }

        //Prepare data to save modified object
        $currentUserObject->name = $requestData['name'];
        $currentUserObject->icon = $requestData['icon'];
        $currentUserObject->stereotype_id = $requestData['stereotype_id'] ? $requestData['stereotype_id'] : null;
        $currentUserObject->type_id = $requestData['type_id'] ? $requestData['type_id'] : null;
        $currentUserObject->subtype_id = $requestData['subtype_id'] ? $requestData['subtype_id'] : null;

        //Save modification
        try {
            $currentUserObject->save();
        } catch (\Exception $e) {
            // Rollback
            DB::rollback();
            return array('error' => 'user_object_modification_not_saved');
        }

        //Commit Transaction
        DB::commit();

        return array('success' => 'success_user_object_modified');

    }
}
