<?php

namespace LeadApi\Http\Controllers;

use LeadApi\Http\Requests;
use LeadApi\SocialMediaNotation;
use LeadApi\ObjectClass;
use LeadApi\ObjectCategory;

class SocialMediaNotationController extends Controller {

    /**
     * Get Social Media Notations List data grouped by Object Category
     * @param string $title
     * @return array
     */
    public function getSocialMediaNotationsList($title){
        $objectClassArr = ObjectClass::where('name','=',urldecode($title))->first();
        if($objectClassArr!= null){
            $socialMediaNotations = ObjectCategory::where('object_class_id','=',$objectClassArr['id'])
                ->select('id','name','object_class_id')
                ->with(array('social_media_notations' => function ($q) {
                    $q->select('id', 'name','description','notation','sub_layer_id','object_category_id');
                },'social_media_notations.sublayer' => function ($q) {
                $q->select('id', 'color');
            }))->get()->toArray();

            if(!empty($socialMediaNotations)){
                return array('success'=>$socialMediaNotations);
            }
        }

        return array('error'=>'no_social_media_notations_exists');

    }

    /**
     * Get Social Media Notation Inner data
     * @param string $title
     * @return mixed
     */
    public function getSocialMediaNotationInnerData($title) {
        return SocialMediaNotation::where('name', '=', urldecode(str_replace('___', '/', $title)))
            ->select('id', 'name', 'description', 'notation', 'sub_layer_id', 'object_category_id')->with(
                array(
                    'sublayer' => function ($q) {
                        $q->select('id', 'name', 'layer_id');
                    },
                    'sublayer.layer' => function ($q) {
                        $q->select('id', 'name');
                    },
                    'object_category' => function ($q) {
                        $q->select('id', 'name', 'object_class_id');
                    },
                    'object_category.object_class' => function ($q) {
                        $q->select('id', 'name');
                    }
                )
            )
            ->first();
    }
}
