<?php

namespace LeadApi\Http\Requests;

use LeadApi\Http\Requests\Request;

class XbpmnNotationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'=>'integer',
            'name'=>'required|string',
            'description'=>'string',
            'object_category_id'=>'required|integer',
        ];
    }
}
