<?php

namespace LeadApi\Http\Requests;

use LeadApi\Http\Requests\Request;
use Illuminate\Http\JsonResponse;

class PermissionsRequest  extends Request {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
        
        ];
    }

    /**
     * Return errors
     * @param array $errors
     * @return JsonResponse
     */
    public function response(array $errors)
    {
            return new JsonResponse(['errors'=>$errors], 422);
    }
}
