<?php

namespace LeadApi\Http\Requests;

use LeadApi\Http\Requests\Request;
use Illuminate\Http\JsonResponse;

class RevertUserObjectChangeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'=>'required|integer',
            'user_id'=>'required|integer',
        ];
    }

    /**
     * Return errors
     * @param array $errors
     * @return JsonResponse
     */
    public function response(array $errors)
    {
        return new JsonResponse(['request_errors'=>$errors], 422);
    }
}
