<?php

namespace LeadApi\Http\Requests;

use LeadApi\Http\Requests\Request;
use Illuminate\Http\JsonResponse;

class UserObjectRequest extends Request {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'name' => 'required|max:255',
            'icon' => 'string|max:255',
            'stereotype_id' => 'numeric',
            'type_id' => 'numeric',
            'subtype_id' => 'numeric',
            'meta_object_id' => 'required|numeric',
            'user_id' => 'required|numeric',
        ];
    }

    /**
     * Return errors
     * @param array $errors
     * @return JsonResponse
     */
    public function response(array $errors)
    {
            return new JsonResponse(['errors'=>$errors], 422);
    }
}
