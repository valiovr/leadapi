<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::resource('test', 'TestController');

Route::group(['middleware' => 'leadapi'], function () {

    Route::get('/', 'MainController@index');
    Route::get('get_user_objects/{userId}', 'UserObjectsController@getUserObjects');
     Route::get('get_user_my_objects/{userId}', 'UserObjectsController@getUserMyObjects');
    Route::get('get_user_instance_objects/{userId}/{instanceId}', 'UserObjectsController@getUserInstanceObjects');
    Route::delete('delete_user_object/{id}/{user_id}', 'UserObjectsController@deleteUserObject');

    Route::resource('user_objects', 'UserObjectsController');
    Route::resource('meta_objects', 'MetaObjectsController');
    Route::get('meta_objects_desc/{id}', 'MetaObjectsController@getDescriptions');
    Route::get('periodic_table', 'PeriodicTableController@index');
    Route::get('semantic_relationships', 'SemanticRelationshipsController@index');
    Route::get('object_relations/{id}', 'SemanticRelationshipsController@getObjectRelations');

    Route::get('get_meta_object_by_id/{id}', 'MetaObjectsController@getMetaObjectById');
    Route::get('get_meta_object_internal_data/{title}', 'MetaObjectsController@getMetaObjectInternalData');
    Route::post('create_new_meta_object_instance', 'MetaObjectsController@createNewMetaObjectInstance');

    //User Object Modifications paths
    Route::delete('delete_user_object_modification/{id}/{user_id}', 'UserObjectChangesController@deleteUserObjectChange');

    //UserObject modification revert
    Route::post('revert_user_object_modification', 'UserObjectChangesController@revertUserObjectChange');

    //User Object modify
    Route::post('user_object_modify', 'UserObjectsController@UserObjectModify');

    //Permissions routes
    Route::get('check_permissions/{id}/{user_id}', 'PermissionsController@checkPermissions');

    //X-BPMN Notations Frontend Routes
    Route::get('get_xbpmn_notation_inner_data/{title}', 'XbpmnNotationsController@getXbpmnNotationInnerData');
    Route::get('get_xbpmn_notation_list/{title}', 'XbpmnNotationsController@getXbpmnNotationsList');

    //BusinessModel Notations Frontend Routes
    Route::get('get_business_model_notation_inner_data/{title}', 'BusinessModelNotationsController@getBusinessModelNotationInnerData');
    Route::get('get_business_model_notation_list/{title}', 'BusinessModelNotationsController@getBusinessModelNotationsList');

    //Object Category Member
    Route::get('get_object_category_members/{title}', 'ObjectCategoryController@getObjectCategoryMembers');

    //Social Media Notations Frontend Routes
    Route::get('get_social_media_notation_inner_data/{title}', 'SocialMediaNotationController@getSocialMediaNotationInnerData');
    Route::get('get_social_media_notation_list/{title}', 'SocialMediaNotationController@getSocialMediaNotationsList');
});


//Admin Routes
Route::group(['prefix' => 'admin','middleware' => 'leadapi'], function () {
    Route::get('get_all_users/{type?}/{order?}', 'Admin\UsersController@getAllUsers');
    Route::get('get_user_data/{user_id}', 'Admin\UsersController@getUserData');
    Route::get('get_all_categories', 'Admin\CategoryController@getAllCategories');
    Route::get('get_all_pages', 'Admin\PagesController@getAllPages');
    Route::post('set_permissions',  'Admin\CategoryController@setPermissions');

    //Meta Objects part
    Route::get('get_meta_objects', 'Admin\MetaObjectsController@getMetaObjects');
    Route::get('get_meta_object_by_id/{id}', 'Admin\MetaObjectsController@getMetaObjectById')->where('id', '[0-9]+');
    Route::post('save_meta_object', 'Admin\MetaObjectsController@saveMetaObject');
    Route::post('save_meta_object_with_notation', 'Admin\MetaObjectsController@saveMetaObjectWithNotation');

    //X-BPMN Notations Routes
    Route::get('get_xbpmn_notations', 'Admin\XbpmnNotationsController@getXbpmnNotations');
    Route::get('get_xbpmn_notations_object_categories', 'Admin\XbpmnNotationsController@getXbpmnNotationsCategories');
    Route::get('get_xbpmn_notation_by_id/{id}', 'Admin\XbpmnNotationsController@getXbpmnNotationById');
    Route::post('add_xbpmn_notation', 'Admin\XbpmnNotationsController@addXbpmnNotation');
    Route::post('edit_xbpmn_notation', 'Admin\XbpmnNotationsController@editXbpmnNotation');

    //Social Media Notations Routes
    Route::get('get_social_media_notations', 'Admin\SocialMediaNotationsController@getSocialMediaNotations');
    Route::get('get_social_media_notations_object_categories', 'Admin\SocialMediaNotationsController@getSocialMediaNotationsCategories');
    Route::get('get_social_media_notation_by_id/{id}', 'Admin\SocialMediaNotationsController@getSocialMediaNotationById');
    Route::post('save_social_media_notation', 'Admin\SocialMediaNotationsController@saveSocialMediaNotation');
});
 
