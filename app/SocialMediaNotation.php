<?php

namespace LeadApi;

use Illuminate\Database\Eloquent\Model;

class SocialMediaNotation extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'la_social_media_notations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','description','notation','sub_layer_id'];

    /**
     * Get Social Media Notation Sub Layer.
     */
    public function sublayer()
    {
        return $this->belongsTo('LeadApi\SubLayer','sub_layer_id');
    }

    /**
     * Get Social Media Notation Object Category.
     */
    public function object_category()
    {
        return $this->belongsTo('LeadApi\ObjectCategory');
    }
}
