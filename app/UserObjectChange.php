<?php

namespace LeadApi;

use Illuminate\Database\Eloquent\Model;

class UserObjectChange extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'la_user_object_changes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','icon','stereotype_id','type_id', 'subtype_id','user_id', 'meta_object_id','user_object_id'];


    /*
     *  Get stereotype of user object
     */
    public function stereotype()
    {
        return $this->belongsTo('LeadApi\ObjectType');
    }
    /*
     *  Get type of user object change
     */
    public function type()
    {
        return $this->belongsTo('LeadApi\ObjectType');
    }
    /*
     *  Get subtype of user object change
     */
    public function subtype()
    {
        return $this->belongsTo('LeadApi\ObjectType');
    }

    /*
     *  Get original meta object of user object change
     */
    public function meta_object()
    {
        return $this->belongsTo('LeadApi\MetaObject');
    }

    /*
     *  Get original meta object of user object change
     */
    public function user_object()
    {
        return $this->belongsTo('LeadApi\UserObject');
    }
}
