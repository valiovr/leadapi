<?php

namespace LeadApi;

use Illuminate\Database\Eloquent\Model;

class MWCategory extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mw_category';


}
