<?php

namespace LeadApi;

use Illuminate\Database\Eloquent\Model;

class BusinessModelNotations extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'la_business_model_notations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','description','notation','sub_layer_id'];

    /**
     * Get meta_object sub_layer.
     */
    public function sublayer()
    {
        return $this->belongsTo('LeadApi\SubLayer','sub_layer_id');
    }

    /**
     * Get meta_object sub_layer.
     */
    public function object_category()
    {
        return $this->belongsTo('LeadApi\ObjectCategory','object_category_id');
    }
}
