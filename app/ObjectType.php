<?php

namespace LeadApi;

use Illuminate\Database\Eloquent\Model;

class ObjectType extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'la_object_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','description','reference','object_id', 'parent_id'];

    /**
     * Self reference relation to parent object types
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo('LeadApi\ObjectType', 'parent_id');
    }

    /**
     * Self reference relation to children object types
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany('LeadApi\ObjectType', 'parent_id');
    }

}
