<?php

namespace LeadApi;

use Illuminate\Database\Eloquent\Model;

class UserObject extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'la_user_objects';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','icon','stereotype_id','type_id', 'subtype_id','user_id', 'meta_object_id'];

    /*
     *  Get stereotype of user object
     */
    public function stereotype()
    {
        return $this->belongsTo('LeadApi\ObjectType');
    }

    /*
     *  Get type of user object
     */
    public function type()
    {
        return $this->belongsTo('LeadApi\ObjectType');
    }
    /*
     *  Get subtype of user object
     */
    public function subtype()
    {
        return $this->belongsTo('LeadApi\ObjectType');
    }

    /*
     *  Get original meta object of user object
     */
    public function meta_object()
    {
        return $this->belongsTo('LeadApi\MetaObject');
    }


    /*
     *  Get all changes of user object
     */
    public function user_object_changes()
    {
        return $this->hasMany('LeadApi\UserObjectChange');
    }
}
