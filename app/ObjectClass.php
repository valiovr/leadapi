<?php

namespace LeadApi;

use Illuminate\Database\Eloquent\Model;

class ObjectClass extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'la_object_classes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Get the Sub-layers for layer.
     */
    public function object_categories()
    {
        return $this->hasMany('LeadApi\ObjectCategory');
    }
}
