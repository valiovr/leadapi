<?php

namespace LeadApi;

use Illuminate\Database\Eloquent\Model;

class ObjectRelation extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'la_object_relations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['object_id','relation_type_id','related_object_id'];

    /**
     * Get the Meta Objects for the relation.
     */
    public function meta_object()
    {
        return $this->belongsTo('LeadApi\MetaObject', 'object_id');
    }

    public function related_object()
    {
        return $this->belongsTo('LeadApi\MetaObject', 'related_object_id');
    }

    public function relation_type()
    {
        return $this->belongsTo('LeadApi\RelationType','relation_type_id');
    }
}
