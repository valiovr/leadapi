<?php

namespace LeadApi;

use Illuminate\Database\Eloquent\Model;

class ObjectCategory extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'la_object_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','object_class_id'];

    /**
     * Get Object Class that owns the Object Category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function object_class()
    {
        return $this->belongsTo('LeadApi\ObjectClass');
    }

    /**
     * Get the Sub-layers for layer.
     */
    public function xbpmn_notations()
    {
        return $this->hasMany('LeadApi\XbpmnNotation');
    }
	
	/**
     * Get the Object category Business Model Notation Members.
     */
    public function business_model_notations()
    {
        return $this->hasMany('LeadApi\BusinessModelNotations');
	}
	
    /**
     * Get the Object category Social Media Notation Members.
     */
    public function social_media_notations()
    {
        return $this->hasMany('LeadApi\SocialMediaNotation');
    }
}
