<?php

namespace LeadApi;

use Illuminate\Database\Eloquent\Model;

class MetaObject extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'la_meta_objects';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','description','icon','l_id'];

    /**
     * Get meta_object sub_layer.
     */
    public function sublayer()
    {
        return $this->belongsTo('LeadApi\SubLayer','sub_layer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function object_relations()
    {
        return $this->hasMany('LeadApi\ObjectRelation','object_id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function user_object()
    {
        return $this->hasMany('LeadApi\UserObject','meta_object_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function stereotype()
    {
        return $this->hasMany('LeadApi\ObjectType','object_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function object_relations_inverse()
    {
        return $this->hasMany('LeadApi\ObjectRelation','related_object_id');
    }


}
