<?php

namespace LeadApi;

use Illuminate\Database\Eloquent\Model;

class RelationType extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'la_relation_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function object_relations()
    {
      return $this->hasMany('LeadApi\ObjectRelation','relation_type_id');
    }
}
