<?php

namespace LeadApi;

use Illuminate\Database\Eloquent\Model;

class MWPage extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mw_page';

    /**
     * Define  table primary key
     *
     * @var string
     */
    protected $primaryKey = 'page_id';

    /**
     * Get the post that owns the comment.
     */
    public function category_links()
    {
        return $this->belongsTo('LeadApi\MWCategoryLink', 'page_id','cl_from');
    }
}
