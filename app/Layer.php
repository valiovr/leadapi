<?php

namespace LeadApi;

use Illuminate\Database\Eloquent\Model;

class Layer extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'la_layers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','description','color'];

    /**
     * Get the Sub-layers for layer.
     */
    public function sublayers()
    {
        return $this->hasMany('LeadApi\SubLayer');
    }
}
