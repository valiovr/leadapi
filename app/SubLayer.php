<?php

namespace LeadApi;

use Illuminate\Database\Eloquent\Model;

class SubLayer extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'la_sub_layers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'color', 'layer_id'];

    /**
     * Get Layer that owns the Sublayer
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function layer()
    {
        return $this->belongsTo('LeadApi\Layer');
    }

    /**
     * Get the Meta Objects for the Sublayer.
     */
    public function meta_objects()
    {
        return $this->hasMany('LeadApi\MetaObject');
    }

}
